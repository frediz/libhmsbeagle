Source: libhmsbeagle
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Tim Booth <tbooth@ceh.ac.uk>,
           Andreas Tille <tille@debian.org>
Section: libs
Priority: optional
Build-Depends: debhelper (>= 12~),
               doxygen,
               graphviz,
               javahelper,
               default-jdk (>= 1:1.6),
               ant,
               dh-linktree,
               dh-exec,
               libjs-jquery,
               ocl-icd-opencl-dev | opencl-dev,
               pocl-opencl-icd [amd64] | opencl-icd [amd64],
               clinfo
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/med-team/libhmsbeagle
Vcs-Git: https://salsa.debian.org/med-team/libhmsbeagle.git
Homepage: https://github.com/beagle-dev/beagle-lib

Package: libhmsbeagle-dev
Architecture: linux-any
Section: libdevel
Depends: libhmsbeagle1v5 (= ${binary:Version}),
         ${shlibs:Depends},
         ${misc:Depends}
Description: High-performance lib for Bayesian and Maximum Likelihood phylogenetics (devel)
 BEAGLE is a high-performance library that can perform the core calculations at
 the heart of most Bayesian and Maximum Likelihood phylogenetics packages. It
 can make use of highly-parallel processors such as those in graphics cards
 (GPUs) found in many PCs.
 .
 The project involves an open API and fast implementations of a library for
 evaluating phylogenetic likelihoods (continuous time Markov processes) of
 biomolecular sequence evolution.
 .
 The aim is to provide high performance evaluation 'services' to a wide range
 of phylogenetic software, both Bayesian samplers and Maximum Likelihood
 optimizers.  This allows these packages to make use of implementations that
 make use of optimized hardware such as graphics processing units.
 .
 This package contains development files needed to build against Beagle library.

Package: libhmsbeagle1v5
Architecture: amd64 i386 arm64 armhf
Depends: ${shlibs:Depends},
         ${misc:Depends},
         beignet-opencl-icd|mesa-opencl-icd|opencl-icd
Pre-Depends: ${misc:Pre-Depends}
Conflicts: libhmsbeagle1
Replaces: libhmsbeagle1
Description: High-performance lib for Bayesian and Maximum Likelihood phylogenetics
 BEAGLE is a high-performance library that can perform the core calculations at
 the heart of most Bayesian and Maximum Likelihood phylogenetics packages. It
 can make use of highly-parallel processors such as those in graphics cards
 (GPUs) found in many PCs.
 .
 The project involves an open API and fast implementations of a library for
 evaluating phylogenetic likelihoods (continuous time Markov processes) of
 biomolecular sequence evolution.
 .
 The aim is to provide high performance evaluation 'services' to a wide range
 of phylogenetic software, both Bayesian samplers and Maximum Likelihood
 optimizers.  This allows these packages to make use of implementations that
 make use of optimized hardware such as graphics processing units.

Package: libhmsbeagle-java
Architecture: linux-any
Section: java
Depends: ${java:Depends},
         ${misc:Depends},
         ${shlibs:Depends},
         libhmsbeagle1v5
Recommends: ${java:Recommends}
Description: High-performance lib for Bayesian and Maximum Likelihood phylogenetics (java)
 BEAGLE is a high-performance library that can perform the core calculations at
 the heart of most Bayesian and Maximum Likelihood phylogenetics packages. It
 can make use of highly-parallel processors such as those in graphics cards
 (GPUs) found in many PCs.
 .
 The project involves an open API and fast implementations of a library for
 evaluating phylogenetic likelihoods (continuous time Markov processes) of
 biomolecular sequence evolution.
 .
 The aim is to provide high performance evaluation 'services' to a wide range
 of phylogenetic software, both Bayesian samplers and Maximum Likelihood
 optimizers.  This allows these packages to make use of implementations that
 make use of optimized hardware such as graphics processing units.
 .
 This package contains the Java interface.
